<?php

namespace Core\Event;

use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;

use Phalcony\Core\Event\SubscriberInterface\AfterDispatch;
use Phalcony\Core\Event\SubscriberInterface\AfterDispatchLoop;
use Phalcony\Core\Event\SubscriberInterface\AfterExecuteRoute;
use Phalcony\Core\Event\SubscriberInterface\BeforeDispatch;
use Phalcony\Core\Event\SubscriberInterface\BeforeDispatchLoop;
use Phalcony\Core\Event\SubscriberInterface\BeforeException;
use Phalcony\Core\Event\SubscriberInterface\BeforeExecuteRoute;
use Phalcony\Core\Event\SubscriberInterface\BeforeNotFoundAction;

/**
 * Class Subscriber
 *
 * Classe de esqueleto exemplo para criação de um Subscriber para os eventos do Phalcon
 * Implemente apenas os listeners necessários de acordo com o evento.
 *
 * Outro exemplo de Listener de eventos pode ser visto no módulo PhalconCas
 *
 * @package Core\Event
 */
class Subscriber implements
    AfterDispatch, AfterDispatchLoop, AfterExecuteRoute,
    BeforeDispatch, BeforeDispatchLoop, BeforeExecuteRoute,
    BeforeException, BeforeNotFoundAction
{
    public function afterDispatch(Event $event, Dispatcher $dispatcher)
    {
    }

    public function afterDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
    }

    public function afterExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
    }

    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
    }

    public function beforeDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
    }

    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
    }

    public function beforeNotFoundAction(Event $event, Dispatcher $dispatcher)
    {
    }

    public function beforeException(Event $event, Dispatcher $dispatcher)
    {
    }
}
