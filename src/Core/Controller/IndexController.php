<?php

namespace Core\Controller;

use Phalcony\Core\Controller\Rest;

/**
 * @RoutePrefix("/")
 */
class IndexController extends Rest
{
    /**
     * @Get("/")
     */
    public function indexAction($data = null)
    {
        $this->response->setJsonContent(
            array('msg' => $this->getDI()->get('translate')->getMessage('OK::000', array('teste', 'ok')))
        );
    }
}
