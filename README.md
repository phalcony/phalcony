Phalcony
===============================================================================

Phalcony aims to be a superset for a better PhalconPHP project. It uses the
PhalconPHP and Zephir language to keep speed and performance high, and, at the
same time, introduce a modern project code organization out-of-the-box.

Phalcony code MUST be organized and implemented on two mainstreams: frontend
and backend.

All backend is REST based, so requests and responses must be, by default, JSON.
There is no template engine, and no HTML render on the server side, which
means... better performance and code focused on application business rules!

Frontend can be implemented using any framework you want: AngularJS, React,
Ember, Backbone, Ionic, Aurelia, Twitter Bootstrap, Materialze, or even
compiled apps for mobile, tablets and desktops!

Some features:

- Completely REST based.
- Completely separated backend and frontend code.
- Compiled code, meaning greater speed and performance for your applications
- Modular code oragnization structure bringing:
    - Separated database migration versioning for each module.
    - Reusable modules: you can pack your modules and reuse on other
      applications throught Composer
- No template engine: focus only on business rules coding on your backend!
- Zerocode concept: just declare a controller with the name of a database model
  and you get full rest!
- Better layer code organization:
    - Business: your business rules.
    - Business Validation layer with a concise API.
    - Controller: only data serialization and output formatting allowed.
    - Migration: database is modular versioned using migrations
    - Model: business entities / database tables
    - Repository: queries and database operations are kept separated from
      the business model
    - Service: annotation based services registration
    - Translation: CSV file modular based translation, easier to maintain -
      just open Excel or Calc.

How to start?
-------------------------------------------------------------------------------------------------------

First you need to install Phalcon (refer https://phalconphp.com/ for details), Phalcony Core extension
and the PHP Yaml library.

To install Yaml library, use the following command:

    $ sudo pecl install yaml

You may be required to install libyaml-devel (REHL/CentOS) or libyaml-dev (Debian/Ubuntu) to install
this extension.

To create a new project you can either download the skeleton zip from Gitlab, or you can use composer:

    $ composer create-project phalcony/phalcony your_project_name [version number]

For Phalcony developers, project creation should be:

    $  composer create-project phalcony/phalcony teste dev-master

Now, just install required vendors with composer.

    $ composer install

Start coding!

PHP Embedded Server
-------------------------------------------------------------------------------------------------------

Blá blá blá

Docker Container
-------------------------------------------------------------------------------------------------------

Blá blá blá

Vagrant
-------------------------------------------------------------------------------------------------------

Blá blá blá

Apache Virtualhost Setup
-------------------------------------------------------------------------------------------------------

Blá blá blá

Documentation
-------------------------------------------------------------------------------------------------------

Phalcony project documentation can be found at https://gitlab.com/phalcony/phalcony-docs

TODO
-------------------------------------------------------------------------------------------------------
- Vagrant image
- Docker image
- Cache mechanism
- Module generator
- Rest crud generator
- Security package
- Login package
- User management package
- Serializer/Deserializer
- Migration for modules