<?php
/**
 * Phalcony Skeleton
 */
use Phalcony\Core\RestApplication;

require_once __DIR__ . '/../vendor/autoload.php';

$application = new RestApplication();
$application->handle();
