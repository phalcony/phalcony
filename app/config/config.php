<?php

/**
 * Get Configuration for Phalcon DevTools only. Has no use in the application, except for DevTools
 */
use Phalcony\Core\RestApplication;

/**
 * Adjust this if you place this file elsewhere
 */
$path = __DIR__ . '/../..';
defined('APP_PATH') || define('APP_PATH', realpath($path));
require_once APP_PATH . '/vendor/autoload.php';

$application = new RestApplication('dev', APP_PATH);
$config      = $application->getConfig();

return $config;
